<!-- @format -->

## Komodo server

```
heroku link : https://nameless-woodland-46424.herokuapp.com
```

# Index

Routes

```
router.get('/', (req, res) => {
    res.status(201).json({
        msg: 'Get Lost!'
    })
})
router.use('/user', userRoutes);
router.use('/board', boardRoutes);
router.use('/list', listRoutes);
router.use('/card', cardRoutes);
router.use('/comment', commentRoutes);

router.post("/register", userControllers.Register);
router.post("/login", userControllers.Login);
```

# User
Routes
```
router.get("/", Authentication, userControllers.GetAll);
router.put("/edit/:id", Authentication, userControllers.Edit);
router.delete("/delete/:id", Authentication, userControllers.Delete);
```

# Board
Routes
```
router.post("/add", Authentication, boardControllers.add);
router.get("/", Authentication, boardControllers.getAll);
router.get("/user/:userId", Authentication, boardControllers.getBoardByUser);
router.put("/edit/:id", Authentication, boardControllers.Edit);
router.delete("/delete/:id", Authentication, boardControllers.Delete);
router.put("/archived/:id", Authentication, boardControllers.Archived);
router.get("/archived/true", Authentication, boardControllers.archivedTrues)
```

# List
Routes
```
router.get("/", Authentication, ListController.getAll);
router.post("/add/:boardId", Authentication, ListController.Add);
router.put("/edit/:id", Authentication, ListController.Edit);
router.delete("/delete/:id", Authentication, ListController.Delete);
router.put("/archived/:id", Authentication, ListController.Archived);
router.get("/archived/true", Authentication, ListController.archivedTrues);
```

# Card
Routes
```
router.post("/add/:listId", Authentication, CardControllers.Add);
router.get("/", Authentication, CardControllers.getAll);
router.put("/edit/:id", Authentication, CardControllers.Edit);
router.delete("/delete/:id", Authentication, CardControllers.Delete);
router.put("/archived/:id", Authentication, CardControllers.Archived);
```

# Comment
Routes
```
router.post("/add/:cardId", Authentication, CommentControllers.Add);
router.get("/", Authentication, CommentControllers.getAll);
router.put("/edit/:id", Authentication, CommentControllers.Edit);
router.delete("/delete/:id", Authentication, CommentControllers.Delete);
```