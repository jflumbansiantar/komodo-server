const { User } = require("../models/user");
const { Board } = require("../models/board");
const mongoose = require("mongoose");

class BoardController {
  static async add(req, res, next) {
    try {
      let userId = req.userData._id
      let obj = {};
      const { boardName, background } = req.body;
    
      if (boardName) obj.boardName = boardName;
      if (background) obj.background = background;
      if (userId) obj.user = userId;
      
      // let found = await Board.findOne({ boardName: boardName });
      
      if (!(await Board.findOne({ boardName: boardName }))) {
        let data = await Board.findOneAndUpdate(
          { _id: mongoose.Types.ObjectId() },
          obj,
          {
            new: true,
            upsert: true,
            runValidators: true,
            setDefaultOnInsert: true
          })
          .populate({ path: 'user', select: ['id', 'fullName'] });
        
        console.log(data);

        res.status(201).json({
          success: true,
          message: "Successfully create a board!",
          data: data,
        });
      } else {
        
        res.status(400).json({
          success: false,
          message: "Board name is already used.",
        });
      }
    } catch (error) {
      next(error);
    }
  };

  static async getAll(req, res, next) {
    try {
      let datas = await Board.find().populate({ path: "user", select: ['_id', 'fullName'] });

      res.status(200).json({
        success: true,
        message: "Successfully retrieve the data!",
        data: datas,
      });
    } catch (error) {
      next(error);
    }
  };

  static async Edit(req, res, next) {
    try {
      const { id } = req.params;
      const userId = req.userData._id;
      console.log(userId);

      if (!id) return next({ message: "Missing ID Params" });
      
      const updatedData = await Board.findByIdAndUpdate(id, { $set: req.body }, { new: true });

      if (userId != updatedData.user)
        return next({ message: "You're not authorized to edit this part." });
      
      res.status(200).json({
        success: true,
        message: "Successfully update board!",
        data: updatedData,
      });
    } catch (error) {
      next(error);
    }
  };

  static async Delete(req, res, next){
    try {
      const { id } = req.params;
      const userId = req.userData._id;
  
      if (!id) return next({ message: "Missing ID Params" });
  
      await Board.findByIdAndRemove(id, (error, doc, res) => {
        if (error) throw "Failed to delete";
        if (!doc) {
          return next(error);
        } else {
          if (userId != doc.user)
            return next({ message: "You're not authorized to  this part." });
      
          return next({
            success: true,
            message: "Successfully delete data!",
            data: doc,
          });
        }

      });
    } catch (error) {
      next(error);
    }
  };

  static async getBoardByUser(req, res, next) {
    try {
      const userId = req.userData;
      let user = await User.findById(userId);
      let data = await Board
        .find({ user: userId })
        .populate({ path: 'user', select: ['fullName', '_id', 'username'] });
      
      res.status(200).json({
        status: true,
        message: `Boards by this ${user.fullName}`,
        data: data
      })

    } catch (error) {
      next(error);
    }
  };

  static async Archived(req, res, next) {
    try {
      let boardId = req.params.id;
      let board = await Board
        .findByIdAndUpdate(boardId, { $set: { archived: true } }, { new: true })
        .populate({ path: 'user', select:['username', 'fullName'] })
      
      res.status(200).json({
        success: true,
        message: 'Archived success!',
        data: board
      })
    } catch (error) {
      next(error)
    }
  };

  static async archivedTrues(req, res, next) {
    try {
      let dataTrues = await Board
        .find({ archived: true })
        .populate({ path: 'user', select: ['username', 'fullName'] })
      res.status(200).json({
        success: true,
        message: 'Archived!',
        data: dataTrues
      })
    } catch (error) {
      next(error)
    }
  }

}

module.exports = BoardController