const { User } = require("../models/user");
const { Board } = require("../models/board");
const { List } = require("../models/list");
const { Card } = require("../models/card");
const mongoose = require("mongoose");

class CardController {
  static async Add(req, res, next) {
    try {
      let obj = {};
      const { title, description } = req.body;
      let userId = req.userData;
      let listId = req.params.listId;
    
      if (title) obj.title = title;
      if (description) obj.description = description;
      if (userId) obj.user = userId;
      if (listId) obj.list = listId;
      
      let found = await Card.findOne({ title: title });
      
      if (!found) {
        let data = await Card.findOneAndUpdate({ _id: mongoose.Types.ObjectId() }, obj,
          { new: true, upsert: true, runValidators: true, setDefaultOnInsert: true })
          .populate({ path: "Lists" })
        console.log(data);

        res.status(201).json({
          success: true,
          message: "Successfully create a card!",
          data: data,
        });
      } else {
        res.status(400).json({
          success: false,
          message: "Card name is already used.",
        });
      }
    } catch (error) {
      next(error);
    }
  };
  
  static async getOne( req, res, next) {
    try {
      const { id } = req.params;

      let data = await Card.findById({ _id: id });
      console.log(data);

      if (!data){
        res.status(400).json({
          status: false,
          message: `Card with boardId:${id} is not found!`,
        })
      } else {
        res.status(400).json({
          status: true,
          message: `Card with boardName: ${data.title}`,
          data: data,
        })
      };
    } catch (error) {
      next(error);
    }
  };

  static async getAll(req, res, next) {
    try {
      let datas = await Card.find()
        .populate({ path: "user", select: ['username', 'fullName'] })
        .populate({
          path: "list", select: ['listName', 'board', 'user'],
          populate: { path: "user", select: ['username', 'fullName'] }
        })

      res.status(200).json({
        success: true,
        message: "Successfully retrieve the data!",
        data: datas,
      });
    } catch (error) {
      next(error);
    }
  };

  static async Edit(req, res, next) {
    try {
      const { id } = req.params;

      if (!id) return next({ message: "Missing ID Params" });

      const updatedData = await Card.findByIdAndUpdate(id, { $set: req.body }, { new: true });

      res.status(200).json({
        success: true,
        message: "Successfully update board!",
        data: updatedData,
      });
    } catch (error) {
      next(error);
    }
  };

  static async Delete(req, res, next){
    try {
      const { id } = req.params;
  
      if (!id) return next({ message: "Missing ID Params" });
  
      await Card.findByIdAndRemove(id, (error, doc, res) => {
        
        if (error) throw "Failed to delete";
        if (!doc) {
          return next(error);
        } else {
          return next({
            success: true,
            message: "Successfully delete data!",
            data: doc,
          });
        }

      });
    } catch (error) {
      next(error);
    }
  };

  static async Archived(req, res, next) {
    try {
      let cardId = req.params.id;
      let card = await Card
        .findByIdAndUpdate(cardId, { $set: { archived: true } }, { new: true })
        .populate({ path: 'List' })
      
      res.status(200).json({
        success: true,
        message: 'Archived success!',
        data: card
      })
    } catch (error) {
      next(error)
    }
  }

  static async GetCardByList(req, res, next) {
    try {
      let listId = req.params.id;
      let card = await Card.findOne({ id: listId });
      
      res.status(200).json({
        success: true,
        message: "Successfully retrieve the data!",
        data: card
      })
    } catch (error) {
      next(error)
    }
  }

}

module.exports = CardController