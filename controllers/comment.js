const { User } = require("../models/user");
const { Board } = require("../models/board");
const { List } = require("../models/list");
const { Card } = require("../models/card");
const { Comment } = require("../models/comment");
const mongoose = require("mongoose");

class CommentController {
  static async Add(req, res, next) {
    try {
      let obj = {};
      const { text } = req.body;
      let userId = req.userData;
      let cardId = req.params.cardId;

      if (text) obj.text = text;
      if (userId) obj.user = userId;
      if (cardId) obj.card = cardId;
      
      let data = await Comment
        .findOneAndUpdate({ _id: mongoose.Types.ObjectId() }, obj,
          { new: true, upsert: true, runValidators: true, setDefaultOnInsert: true, populate: { path: 'card' } })
        .populate({ path: 'user' });
      
        console.log(data);

        res.status(201).json({
          success: true,
          message: "Successfully create a comment!",
          data: data,
        });
      
    } catch (error) {
      next(error);
    }
  };

  static async getAll(req, res, next) {
    try {
      let datas = await Comment.find()
        .populate({ path: "card", select: ['title', 'description'] })
        .populate({ path: "user", select: ['username', 'fullName'] });
      console.log(datas);

      res.status(200).json({
        success: true,
        message: "Successfully retrieve the data!",
        data: datas,
      });
    } catch (error) {
      next(error);
    }
  };

  static async Edit(req, res, next) {
    try {
      const { id } = req.params;

      if (!id) return next({ message: "Missing ID Params" });

      const updatedData = await Comment.findByIdAndUpdate(id, { $set: req.body }, { new: true });
      console.log(updatedData);

      res.status(200).json({
        success: true,
        message: "Successfully update board!",
        data: updatedData,
      });
    } catch (error) {
      next(error);
    }
  };

  static async Delete(req, res, next){
    try {
      const { id } = req.params;
  
      if (!id) return next({ message: "Missing ID Params" });
  
      await Comment.findByIdAndRemove(id, (error, doc, res) => {

        if (error) throw "Failed to delete";
        if (!doc) {
          return next(error);
        } else {
          return next({
            success: true,
            message: "Successfully delete data!",
            data: doc,
          });
        }

      });
    } catch (error) {
      next(error);
    }
  };

}

module.exports = CommentController