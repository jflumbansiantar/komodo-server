const { User } = require("../models/user");
const { List }  = require("../models/list");
const { Board } = require("../models/board");

const mongoose = require("mongoose");
// console.log(Board, '-iki naon?');

class ListController {
  static async Add(req, res, next) {
    try {
      const userId = req.userData;
      const boardId = req.params.boardId;

      const boards = await Board.find( {boardId} );
      console.log(boards)

         //validate
            // let ticketIds = orderTicket.map(el => mongoose.Types.ObjectId(el.ticketId))
            // let ticketData =await Ticket.find({_id: ticketIds})
            // .select('eventTitle');

            //Check the transaction.
            // let checkEvent = ticketData.every( (val, i, arr) => val === arr[0] )
            // if(!checkEvent){
            //     return res.send('Cannot purchase ticket of different Event.')
            // }
      
      let obj = {};
      const { listName } = req.body;
    
      if (listName) obj.listName = listName;
      if (boardId) obj.board = boardId;
      if (userId) obj.user = userId;

      let found = await List.findOne({ listName: listName })
      
      if (!found) {
        let data = await List
          .findOneAndUpdate({ _id: mongoose.Types.ObjectId() }, obj,
            { new: true, upsert: true, runValidators: true, setDefaultOnInsert: true, populate: {path: 'Board'}})
          // .populate({ path: 'boards' })
          // .populate({ path: 'user', select: ['username', 'fullName'] })
        
        res.status(201).json({
          success: true,
          message: "Successfully create a list!",
          data: data,
        });
      } else {
        res.status(400).json({
          success: false,
          message: 'Failed to create a list!'
        });
      }

    } catch (error) {
      next(error);
    }
  };

  static async getAll(req, res, next) {
    try {
      let datas = await List.find()
        .populate({ path: "Board", select: ['user', 'boardName'] })
        .populate({ path: 'user', select: ['username', 'fullName'] });
      console.log(datas);

      res.status(200).json({
        success: true,
        message: "Successfully retrieve the data!",
        data: datas,
      });
    } catch (err) {
      next(err);
    }
  };

  static async Edit(req, res, next) {
    try {
      const { id } = req.params;
      const userId = req.userData._id;
      console.log(userId);

      if (!id) return next({ message: "Missing ID Params" });

      const updatedData = await List.findByIdAndUpdate(id, { $set: req.body }, { new: true });
      
      console.log(updatedData);

      if (userId != updatedData.board.user)
        return next({ message: "You're not authorized to edit this part." });
      
      res.status(200).json({
        success: true,
        message: "Successfully updated data!",
        data: updatedData,
      });
    } catch (err) {
      next(err);
    }
  };

  static async Delete(req, res, next){
    try {
      const { id } = req.params;
      const userId = req.userData._id;
      console.log(userId);
  
      if (!id) return next({ message: "Missing ID Params" });
  
      await List.findByIdAndRemove(id, (error, doc, result) => {
        if (error) throw "Failed to delete";
        if (!doc)
          return res.status(400).json({
            success: false,
            err: error.message
          });
          console.log(doc);
        res.status(200).json({
          success: true,
          message: "Successfully delete data!",
          data: doc,
        });
      });
    } catch (err) {
      next(err);
    }
  };

  static async Archived(req, res, next) {
    try {
      let listId = req.params.id;
      let list = await List
        .findByIdAndUpdate(listId, { $set: { archived: true } }, {new: true} )
        .populate({ path: 'Board' })
      
      res.status(200).json({
        success: true,
        message: 'Archived success!',
        data: list
      })
    } catch (error) {
      next(error)
    }
  }

  static async archivedTrues(req, res, next) {
    try {
      let dataTrues = await List.find({ archived: true }).populate({ path: 'user', select: ['username', 'fullName'] })
      res.status(200).json({
        success: true,
        message: 'Archived!',
        data: dataTrues
      })
    } catch (error) {
      next(error)
    }
  }
}

module.exports = { ListController }
