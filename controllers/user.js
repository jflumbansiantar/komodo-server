const { User } = require("../models/user");

const { decryptPassword } = require("../helpers/bcrypt");
const { tokenGenerator } = require("../helpers/jwt");

class userController {
  static async Register(req, res, next) {
    try {
      //check
      let obj = {};
      const { email, password, username } = req.body;
    
      if (email) obj.email = email;
      if (password) obj.password = password;
      if (username) obj.username = username;
    
      let foundUsername = await User.findOne({ username: username });
      let foundEmail = await User.findOne({ email: email });
    
      if (!foundUsername && !foundEmail) {
        let data = await User.create(obj);
        console.log(data);

        res.status(201).json({
          success: true,
          message: "Successfully create a user!",
          data: data,
        });
      } else {
        console.log('Hello');
        res.status(400).json({
          success: false,
          message: "Username or email already used.",
        });
      }
    } catch (err) {
      next(err);
    }
  };
  
  static async Login(req, res, next) {
    try {
      const { username, password } = req.body;

      let data = await User.findOne({ username: username });
      console.log(data);

      if (!data)
        return next({
          message: `User with username:${username} is not found!`,
        });

      if (decryptPassword(password, data.password)) {
        const token = tokenGenerator(data);

        res.status(200).json({
          success: true,
          message: "Successfully logged in!",
          token: token,
        });
      }
    } catch (error) {
      next(error);
    }
  };

  static async GetAll(req, res, next) {
    try {
      let datas = await User.find().select("-password").populate({
        path: "User",
      });
      console.log(datas);
      res.status(200).json({
        success: true,
        message: "Successfully retrieve the data!",
        data: datas,
      });
    } catch (err) {
      next(err);
    }
  };

  static async Edit(req, res, next) {
    try {
      const { id } = req.params;

      if (!id) return next({ message: "Missing ID Params" });

      const updatedData = await User.findByIdAndUpdate(
        id,
        { $set: req.body },
        { new: true }
      );
      console.log(updatedData);
      res.status(200).json({
        success: true,
        message: "Successfully update profile!",
        data: updatedData,
      });
    } catch (err) {
      next(err);
    }
  };

  static async Delete(req, res, next){
    try {
      const { id } = req.params;
  
      if (!id) return next({ message: "Missing ID Params" });
  
      await User.findByIdAndRemove(id, (error, doc, result) => {
        if (error) throw "Failed to delete";
        if (!doc)
          return res.status(400).json({
            success: false,
            err: error.message
          });
          console.log(doc);
        res.status(200).json({
          success: true,
          message: "Successfully delete data!",
          data: doc,
        });
      });
    } catch (err) {
      next(err);
    }
  };

  static async GetMine(req, res, next) {
    try {
      const { id } = req.params;
      if (!id) return next({ message: "Missing ID Params" });
      
      let data = await User.findById(id).select("-password").populate({
        path: "User",
      });

      // console.log(data);
      res.status(200).json({
        success: true,
        message: "Successfully retrieve the data!",
        data: data,
      });
    } catch (err) {
      next(err);
    }
  };

}

module.exports = userController