const multer = require('multer');
const cloudinary = require('cloudinary');
const { CloudinaryStorage } = require('multer-storage-cloudinary');

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET,
});

const storage = new CloudinaryStorage({
    cloudinary: cloudinary,
    folder: "test123",
    allowedFormats: ["jpg", "jpeg", "png", "gif", "svg"],
    filename: (req, files, cb) => {
        cb(null, Date.now() + "_" + files.originalName.split(".")[0]);
    }
});

const uploader = multer({
    storage: storage,
})

module.exports = { uploader };