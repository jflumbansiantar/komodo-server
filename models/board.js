const mongoose = require("mongoose");
const { Schema } = mongoose;

const boardSchema = new Schema(
    {
        boardName: {
            type: String,
            required: true,
            minlength: 4,
            trim: true,
        },
        background: {
            type: String,
            required: true,
        },
        archived: {
            type: Boolean,
            required: true,
            default: false
        },
        user: { type: Schema.Types.ObjectId, ref: "User" },
        
    }, { timestamps: true, versionKey: false }
);

// const board = ;

exports.Board = mongoose.model("Board", boardSchema);
