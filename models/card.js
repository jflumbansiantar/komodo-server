const mongoose = require("mongoose");
const { Schema } = mongoose;

const cardSchema = new Schema(
    {
        title: {
            type: String,
            required: true,
            minlength: 4,
        },
        description: {
            type: String,
            required: true,
            default: null,
        },
        list: {
            type: Schema.Types.ObjectId,
            ref: 'List'
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        archived: {
            type: Boolean,
            required: true,
            default: false
        },
        
    }, { timestamps: true, versionKey: false }
);

const card = mongoose.model("Card", cardSchema);

exports.Card = card;
