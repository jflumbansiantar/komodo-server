const mongoose = require("mongoose");
const { Schema } = mongoose;

const commentSchema = new Schema(
    {
        text: {
            type: String,
            required: true,
        },
        card: {
            type: Schema.Types.ObjectId,
            ref: 'Card'
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        
    }, { timestamps: true, versionKey: false }
);

const comment = mongoose.model("Comment", commentSchema);

exports.Comment = comment;
