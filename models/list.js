const mongoose = require("mongoose");
const { Schema } = mongoose;

const listSchema = new Schema(
    {
        listName: {
            type: String,
            required: true,
            minlength: 4,
            trim: true,
        },
        archived: {
            type: Boolean,
            required: true,
            default: false
        },
        board: {
            type: Schema.Types.ObjectId,
            ref: 'Board'
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
        
    }, { timestamps: true, versionKey: false }
);

exports.List = mongoose.model("List", listSchema);
