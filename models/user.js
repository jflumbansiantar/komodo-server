const mongoose = require("mongoose");
const { Schema } = mongoose;
const uniqueValidator = require("mongoose-unique-validator");

// hashing password
const { encryptPassword } = require("../helpers/bcrypt");

const userSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      lowercase: true,
      minlength: 4,
      trim: true,
      uniqueCaseInsensitive: true,
    },
    password: {
      type: String,
      required: true,
      match: [/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, "Password minimum eight characters, at least one letter and one number"],
    },
    fullName: {
      type: String,
      sentencecase: true,
      trim: true,
      default: "Anonymous",
    },
    profilePhoto: {
      type: String,
      default:
        "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
    },
    mobileNumber: {
      type: String,
      match: [
        /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$/,
        "Please input in mobile number format, example:  (+44)(0)20-12341234 or 02012341234 or +44 (0) 1234-1234 ",
      ],
    },
    email: {
      type: String,
      required: true,
    },
  },
  { timestamps: true, versionKey: false }
);

// pre, post hooks
userSchema.pre("save", async function (next) {
  let user = this;

  if (user.password && user.isModified("password")) {
    user.password = await encryptPassword(user.password);
  }
  next();
});

// unique validator
userSchema.plugin(uniqueValidator);

// const user = 

exports.User = mongoose.model("User", userSchema);;
