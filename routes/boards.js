const express = require("express");
const router = express.Router();

const boardControllers = require("../controllers/board");
const {Authentication, isOwner, isMine } = require("../middlewares/auth");

router.post("/add", Authentication, boardControllers.add);
router.get("/", Authentication, boardControllers.getAll);
router.get("/user/:userId", Authentication, boardControllers.getBoardByUser);
router.put("/edit/:id", Authentication, boardControllers.Edit);
router.delete("/delete/:id", Authentication, boardControllers.Delete);
router.put("/archived/:id", Authentication, boardControllers.Archived);
router.get("/archived/true", Authentication, boardControllers.archivedTrues)

module.exports = router;
