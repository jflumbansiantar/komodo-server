const express = require("express");
const router = express.Router();

const CardControllers = require("../controllers/card");
const { Authentication } = require("../middlewares/auth");

router.post("/add/:listId", Authentication, CardControllers.Add);
router.get("/", Authentication, CardControllers.getAll);
router.put("/edit/:id", Authentication, CardControllers.Edit);
router.delete("/delete/:id", Authentication, CardControllers.Delete);
router.put("/archived/:id", Authentication, CardControllers.Archived);

module.exports = router;


//PERMASALAHAN DI AUTHENTICATIONNYA!!!!!!!