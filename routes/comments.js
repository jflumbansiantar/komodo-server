const express = require("express");
const router = express.Router();

const CommentControllers = require("../controllers/comment");
const { Authentication } = require("../middlewares/auth");

router.post("/add/:cardId", Authentication, CommentControllers.Add);
router.get("/", Authentication, CommentControllers.getAll);
router.put("/edit/:id", Authentication, CommentControllers.Edit);
router.delete("/delete/:id", Authentication, CommentControllers.Delete);

module.exports = router;
