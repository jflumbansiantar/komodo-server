const express = require("express");
const router = express.Router();
const userRoutes = require("./users");
const boardRoutes = require("./boards");
const listRoutes = require("./lists");
const cardRoutes = require("./cards");
const commentRoutes = require("./comments");
const userControllers = require("../controllers/user");

router.use('/user', userRoutes);
router.use('/board', boardRoutes);
router.use('/list', listRoutes);
router.use('/card', cardRoutes);
router.use('/comment', commentRoutes);

router.post("/register", userControllers.Register);
router.post("/login", userControllers.Login);

router.get('/', (req, res) => {
    res.status(201).json({
        msg: 'Get Lost!'
    })
})

module.exports = router;