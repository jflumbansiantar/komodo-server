const express = require("express");
const router = express.Router();

const { ListController } = require("../controllers/list");
const {Authentication } = require("../middlewares/auth");

router.get("/", Authentication, ListController.getAll);
router.post("/add/:boardId", Authentication, ListController.Add);
router.put("/edit/:id", Authentication, ListController.Edit);
router.delete("/delete/:id", Authentication, ListController.Delete);
router.put("/archived/:id", Authentication, ListController.Archived);
router.get("/archived/true", Authentication, ListController.archivedTrues);

module.exports = router;
