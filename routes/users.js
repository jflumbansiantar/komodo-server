const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/user");
const { Authentication } = require("../middlewares/auth");

router.get("/", Authentication, userControllers.GetAll);
router.put("/edit/:id", Authentication, userControllers.Edit);
router.delete("/delete/:id", Authentication, userControllers.Delete);

// router.get("/:id", Authentication, isMine, userControllers.GetMine);

module.exports = router;
